//
//  FavDetailViewController.swift
//  TvShows
//
//  Created by Joel Lozano on 10/10/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import UIKit
import CoreData

class FavDetailViewController: UIViewController, UITabBarDelegate, UITableViewDataSource {
  
    var favsCollection = [TvShowsFav]()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.red
        
        self.tableView.reloadData()
        getData()

    }
    
    func getData(){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let shows = NSFetchRequest<NSFetchRequestResult>(entityName: "TvShowsFavs")
        
        
        do {
            let results = try managedContext.fetch(shows)
            for data in results as! [NSManagedObject]{
                print(data.value(forKey: "name") as! String)
                
                let structTvShow  = TvShowsFav(name: data.value(forKey: "name") as! String, image: data.value(forKey: "imageTvShow") as! String, genres: [data.value(forKey: "genre") as! String], externals: data.value(forKey: "imdb") as! String, language: data.value(forKey: "languaje") as! String, summary: data.value(forKey: "summary") as! String)

                
                favsCollection.append(structTvShow)
        }
        }catch  {
            print("No ha sido posible cargar")
        }
        tableView.reloadData()
    }
        
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favsCollection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavCell") as? FavCellTableViewCell
        cell?.tvFav.setImage(UIImage(named: "1380-200"), for: .normal)
        cell?.tvName.text = favsCollection[indexPath.row].name
        if let imageURL = URL(string: (favsCollection[indexPath.row].image!)){
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: imageURL)
                let image = UIImage(data: data!)
                DispatchQueue.main.async {
                    if let image = image{
                        cell!.TvImage.image = image
                    }else{
                        cell!.TvImage.image = UIImage(named: "descarga")
                    }
                    
                }
            }
        }
        return cell!
    }

}
