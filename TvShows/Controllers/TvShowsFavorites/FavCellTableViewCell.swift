//
//  FavCellTableViewCell.swift
//  TvShows
//
//  Created by Joel Lozano on 10/10/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import UIKit

class FavCellTableViewCell: UITableViewCell {

    @IBOutlet weak var tvFav: UIButton!
    @IBOutlet weak var tvName: UILabel!
    @IBOutlet weak var TvImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    @IBAction func actionFav(_ sender: Any) {
    }
    
}
