//
//  ViewController.swift
//  TvShows
//
//  Created by Joel Lozano on 10/9/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tvShowsArray = [TvShowsStruct]()

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.blue
        
        tableView.separatorColor = UIColor(white: 0.5, alpha: 0.5)
        let jsonUrl = "http://api.tvmaze.com/shows"
        guard let url = URL(string: jsonUrl) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else {return}

            do{
                let tvshow = try JSONDecoder().decode([TvShowsStruct].self, from: data)
                self.tvShowsArray = tvshow
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        }.resume()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tvShowsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CellTableViewCell
        cell.tvShowName.text  = tvShowsArray[indexPath.row].name
        if let imageURL = URL(string: (tvShowsArray[indexPath.row].image?.medium!)!){
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: imageURL)
                let image = UIImage(data: data!)
                DispatchQueue.main.async {
                    if let image = image{
                        cell.tvShowImage.image = image
                    }else{
                        cell.tvShowImage.image = UIImage(named: "descarga")
                    }
                    
                }
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        // Favoritos
        let favoriteAction = UITableViewRowAction(style: .default, title: "Favorito") {
            (option, indexPath) in
            let message = "Se ha guadado \(String(describing: self.tvShowsArray[indexPath.row].name!)) a mis favoritos"
            self.saveTvvShowAsFav(tv: self.tvShowsArray[indexPath.row])
            self.showAlert(message: message)
        
        }
        
        return [favoriteAction]
    }
    
    func saveTvvShowAsFav(tv:TvShowsStruct ){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "TvShowsFavs", in: managedContext)
        let newShow = NSManagedObject(entity: entity!, insertInto: managedContext)
        
        newShow.setValue(tv.name, forKey: "name")
        newShow.setValue(tv.image?.medium!, forKey: "imageTvShow")
        newShow.setValue(tv.genres![0], forKey: "genre")
        newShow.setValue(tv.language, forKey: "languaje")
        newShow.setValue(tv.externals?.imdb, forKey: "imdb")
        newShow.setValue(tv.summary, forKey: "summary")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("No ha sido posible guardar \(error), \(error.userInfo)")
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "DetailTvShow", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "DetailTvShow"{
            if let indexPath = self.tableView.indexPathForSelectedRow{
                let selectedTvShow = self.tvShowsArray[indexPath.row]
                let destinationViewController = segue.destination as! tvShowDetailViewController
                destinationViewController.arrayTVShows = selectedTvShow
            }
        }
       
    }
    
    func showAlert(message: String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    


}

