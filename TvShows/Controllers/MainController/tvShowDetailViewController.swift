//
//  tvShowDetailViewController.swift
//  TvShows
//
//  Created by Joel Lozano on 10/9/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import UIKit

class tvShowDetailViewController: UIViewController {
    
    @IBOutlet weak var favButton: UIButton!
    var arrayTVShows : TvShowsStruct!
    
    @IBOutlet weak var tvShowButton: UIButton!
    @IBOutlet weak var tvShowTitle: UILabel!
    @IBOutlet weak var tvShowImage: UIImageView!
    @IBOutlet weak var tvShowGenre: UILabel!
    @IBOutlet weak var tvShowLanguaje: UILabel!
    @IBOutlet weak var tcShowSummary: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        favButton.tintColor = .red
        favButton.setImage(UIImage.init(named: "1308-200"), for: .normal)
        favButton.addTarget(self, action: #selector(markAsFavorite), for: .touchUpInside)
        
        
        
        if let url = self.arrayTVShows.externals!.imdb {
            tvShowButton.setTitle(url, for: .normal)
            tvShowButton.isEnabled = true
        }else {
            tvShowButton.isEnabled = false
        }
        
        
        self.tvShowTitle.text = self.arrayTVShows.name!
        
        if let imageURL = URL(string: self.arrayTVShows.image!.medium!){
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: imageURL)
                let image = UIImage(data: data!)
                DispatchQueue.main.async {
                    if let image = image{
                        self.tvShowImage.image = image
                    }else{
                        self.tvShowImage.image = UIImage(named: "descarga")
                    }
                    
                }
            }
        }
        self.tvShowGenre.text = self.arrayTVShows.genres![0]
        self.tvShowLanguaje.text = self.arrayTVShows.language
        self.tvShowButton.titleLabel?.text = self.arrayTVShows.externals?.imdb
        self.tcShowSummary.text = self.arrayTVShows.summary
        
        
     

    }
    
    @objc func markAsFavorite(){
        print("fav")
    }

    
    
    @IBAction func didTapMDB(_ sender: Any) {
        
        
        if let url = self.arrayTVShows.externals!.imdb {
            
            let urlOk = "https://www.imdb.com/title/\(url)"
            UIApplication.shared.openURL(URL(string: urlOk)!)
        }else {
            return
        }
        
       
    }
    

}
