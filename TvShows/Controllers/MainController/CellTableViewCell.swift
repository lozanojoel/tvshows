//
//  CellTableViewCell.swift
//  TvShows
//
//  Created by Joel Lozano on 10/9/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import UIKit

class CellTableViewCell: UITableViewCell {

    @IBOutlet weak var tvShowImage: UIImageView!
    
    @IBOutlet weak var tvShowName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
