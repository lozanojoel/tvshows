//
//  TvShoesData.swift
//  TvShows
//
//  Created by Joel Lozano on 10/9/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import Foundation

struct TvShowsStruct : Decodable {
    var name : String?
    var image : imageURL?
    var genres : [String]?
    var externals : Externals?
    var language : String?
    var summary : String?
    
    init(name: String, image: imageURL, genres: [String], externals: Externals, language: String, summary: String) {
        self.name = name
        self.image = image
        self.genres = genres
        self.externals = externals
        self.language = language
        self.summary = summary
    }
}

struct imageURL : Decodable{
    var medium : String?
    var original : String?
}

struct Externals : Decodable{
    var tvrage : Int?
    var thetvdb : Int?
    var imdb : String?
}

struct TvShowsFav : Decodable {
    var name : String?
    var image : String?
    var genres : [String]?
    var externals : String?
    var language : String?
    var summary : String?
    
    init(name: String, image: String, genres: [String], externals: String, language: String, summary: String) {
        self.name = name
        self.image = image
        self.genres = genres
        self.externals = externals
        self.language = language
        self.summary = summary
    }
}
